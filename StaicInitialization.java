import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Main {


  static Scanner sc=new Scanner(System.in);
            
  public static int B = sc.nextInt();
  public static int H = sc.nextInt();
  public static boolean flag = isPositive(B, H);
  
  public static boolean isPositive(int x, int y){
    if (x <= 0 || y<=0){
      System.out.println("java.lang.Exception: Breadth and height must be positive");
      return false;
    }
    return true;
  }



public static void main(String[] args){
		if(flag){
			int area=B*H;
			System.out.print(area);
		}
		
	}//end of main

}//end of class


